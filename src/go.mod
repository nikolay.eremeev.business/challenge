module example.com/m/v2

go 1.17

require github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d

require (
	code.cloudfoundry.org/go-diodes v0.0.0-20210909174843-33ea1b777686 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190411002643-bd77b112433e // indirect
)
